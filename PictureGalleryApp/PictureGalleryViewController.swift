import UIKit

class PictureGalleryViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var backButtonView: UIButton!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var likeView: UIImageView!
    @IBOutlet weak var commentView: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var zoomButtonView: UIButton!
    
    // MARK: - let
    let rightBackgroundView = UIImageView()
    let leftBackgroundView = UIImageView()
    let unlikedImage = UIImage(named: "NotFavorite")
    let likedUmage = UIImage(named: "Favorite")
    
    // MARK: - var
    var picturesArray = Manager.shared.loadData()
    var photoArray: [UIImage] = []
    var number = 0
    var commentText: String?
    var isLiked = false
    
    // MARK: - VC life Func
    override func viewDidLoad() {
        super.viewDidLoad()
        self.blurView.isHidden = true
        self.createLikeButton()
        self.loadFirstImage()
        self.loadImages()
        self.createImageView()
        self.mainImageView.dropShadow()
        self.registerForKeyboardNotifications()
        self.commentView.delegate = self
        self.backButtonView.cornerRadius()
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.scrollView.contentInset = UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapDetected(_:)))
        blurView.addGestureRecognizer(tapRecognizer)
        
        let recognizerRight = UISwipeGestureRecognizer(target: self, action: #selector(swipeRightDetected(_:)))
        recognizerRight.direction = .right
        recognizerRight.numberOfTouchesRequired = 1
        view.addGestureRecognizer(recognizerRight)
        
        let recognizerLeft = UISwipeGestureRecognizer(target: self, action: #selector(swipeLeftDetected(_:)))
        recognizerLeft.direction = .left
        recognizerLeft.numberOfTouchesRequired = 1
        view.addGestureRecognizer(recognizerLeft)
    }
    // MARK: - IBAction
    
    @IBAction private func keyboardWillShow(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo,
              let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue,
              let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        
        if notification.name == UIResponder.keyboardWillShowNotification  {
            self.view.frame.origin.y = -keyboardScreenEndFrame.height
        } else {
            self.view.frame.origin.y = 0
        }
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func swipeLeftDetected(_ recognizer: UISwipeGestureRecognizer) {
        self.createLeftBackgroundImage()
        self.number = self.getPrevIndex()
        self.mainImageView.image = self.photoArray[self.number]
        self.commentLabel.text = ""
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut) {
            self.leftBackgroundView.frame.origin.x -= self.view.frame.size.width * 2
        } completion: { (_) in
            self.checkLike()
            self.checkComment()
            self.leftBackgroundView.removeFromSuperview()
        }
    }
    
    @IBAction func swipeRightDetected(_ Recognizer: UISwipeGestureRecognizer) {
        self.createRightBackgroundImage()
        self.number = self.getNextIndex()
        self.commentLabel.text = ""
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut) {
            self.rightBackgroundView.frame.origin.x -= self.view.frame.size.width * 2
        } completion: { (_) in
            self.mainImageView.image = self.photoArray[self.number]
            self.checkLike()
            self.checkComment()
            self.rightBackgroundView.removeFromSuperview()
        }
    }
    
    @IBAction func likeButtonPressed(_ sender: UIButton) {
        if picturesArray[number].like == true {
            picturesArray[number].like = false
            likeView.image = unlikedImage
            isLiked = false
            saveLike()
        } else {
            picturesArray[number].like = true
            likeView.image = likedUmage
            isLiked = true
            saveLike()
        }
        
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tapDetected(_ Recognizer: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut) {
            if self.blurView.isHidden == false {
                self.blurView.isHidden = true
                
                self.mainImageView.frame.origin.x = 44
                self.mainImageView.frame.size.width -= 100
                self.mainImageView.frame.size.height -= 50
                self.mainImageView.contentMode = .scaleToFill
            }
        }completion: { (_) in
            self.zoomButtonView.isEnabled = true
        }
    }
    
    @IBAction func zoomPhoto(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut) {
            self.blurView.isHidden = false
            
            self.mainImageView.contentMode = .scaleToFill
            self.mainImageView.frame.origin.x = 0
            self.mainImageView.frame.size.width += 100
            self.mainImageView.frame.size.height += 50
        } completion: { (_) in
            self.zoomButtonView.isEnabled = false
        }
    }
    
    // MARK: - Flow func
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func createImageView () {
        mainImageView.frame = CGRect(x: self.mainImageView.frame.origin.x,
                                     y: self.mainImageView.frame.origin.y,
                                     width: self.mainImageView.frame.size.width,
                                     height: self.mainImageView.frame.size.height)
        mainImageView.contentMode = .scaleToFill
        mainImageView.image = self.photoArray[self.number]
    }
    
    func getNextIndex() -> Int {
        if self.number == self.photoArray.count - 1 {
            return 0
        }
        return self.number + 1
    }
    
    func getPrevIndex() -> Int {
        if self.number == 0 {
            return self.photoArray.count - 1
        }
        return self.number - 1
    }
    
    func createRightBackgroundImage () {
        rightBackgroundView.frame = CGRect(x: self.view.frame.size.width * 2,
                                           y: self.mainImageView.frame.origin.y,
                                           width: self.mainImageView.frame.size.width,
                                           height: self.mainImageView.frame.size.height)
        rightBackgroundView.contentMode = .scaleToFill
        rightBackgroundView.image = self.photoArray[self.getNextIndex()]
        view.addSubview(rightBackgroundView)
    }
    
    func createLeftBackgroundImage () {
        leftBackgroundView.frame = CGRect(x: self.mainImageView.frame.origin.x,
                                          y: self.mainImageView.frame.origin.y,
                                          width: self.mainImageView.frame.size.width,
                                          height: self.mainImageView.frame.size.height)
        leftBackgroundView.contentMode = .scaleToFill
        leftBackgroundView.image = self.photoArray[self.number]
        view.addSubview(leftBackgroundView)
    }
    
    func createLikeButton() {
        likeView.image = UIImage(named: "NotFavorite")
    }
    
    func saveComment() {
        picturesArray[number].comment = self.commentLabel.text
        UserDefaults.standard.set(encodable: picturesArray, forKey: Keys.imageInfo.rawValue)
    }
    
    func saveLike() {
        picturesArray[number].like = isLiked
        UserDefaults.standard.set(encodable: picturesArray, forKey: Keys.imageInfo.rawValue)
    }
    
    
    func loadImages() {
        let manager = FileManagerClass()
        let pictures = picturesArray.map({$0.picture})
        for selectedPicture in pictures {
            if let convertedPicture = manager.loadImage(fileName: selectedPicture!) {
                photoArray.append(convertedPicture)
            }
        }
    }
    
    func loadFirstImage() {
        if let imageName = picturesArray[number].picture {
            let manager = FileManagerClass()
            self.mainImageView.image = manager.loadImage(fileName: imageName)
            let comment = picturesArray[number].comment
            self.commentLabel.text = comment
            checkLike()
        }
    }
    
    func checkLike() {
        let like = picturesArray[number].like
        if like == false {
            isLiked = false
            likeView.image = unlikedImage
        } else {
            isLiked = true
            likeView.image = likedUmage
        }
    }
    
    func checkComment() {
        guard let comment = picturesArray[number].comment else { return }
        if comment != nil {
            commentLabel.text = comment
        } else {
            commentLabel.text = ""
        }
    }
}
// MARK: - Extensions
extension PictureGalleryViewController: UITextFieldDelegate  {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        commentText = commentView.text
        commentLabel.text = commentText
        commentView.text = ""
        self.saveComment()
        commentView.resignFirstResponder()
        return true
    }
}

extension UIView {
    
    func dropShadow(_ radius: CGFloat = 15) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 10, height: 10)
        layer.shadowRadius = 20
        layer.cornerRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
    }
}
