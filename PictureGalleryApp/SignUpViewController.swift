import UIKit
import SwiftyKeychainKit

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var goNextButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var informationLabel: UILabel!
    
    let keychain = Keychain(service: "com.veronika.PictureGalleryApp")
    let accessTokenKey = KeychainKey<String>(key: "accessToken")
    
    var passwordsMatch = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setButtonView()
        self.passwordTextField.isSecureTextEntry = true
        self.confirmPasswordTextField.isSecureTextEntry = true
    }
    
    @IBAction func goToGallery(_ sender: UIButton) {
        let email = emailTextField.text!
        print(isValidEmail(email))
        if isValidEmail(email) == false {
            self.informationLabel.text = "Invalid email adress"
        }
        guard let password = passwordTextField.text else {return}
        if isPasswordsMatch() && isValidEmail(email) == true {
            do {
                try keychain.set(password, for: accessTokenKey)
            } catch let error {
                debugPrint(error)
            }
            
            let controller = storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            self.navigationController?.pushViewController(controller, animated: true)
            self.informationLabel.text = "Correct!"
        } else {
            
        }
    }
    
    @IBAction func goToLogInScreen(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    @IBAction func showPassword(_ sender: UIButton) {
        self.passwordTextField.isSecureTextEntry = !self.passwordTextField.isSecureTextEntry
    }
    
    @IBAction func showConfirmedPassword(_ sender: UIButton) {
        self.confirmPasswordTextField.isSecureTextEntry = !self.confirmPasswordTextField.isSecureTextEntry
    }
    
    func isPasswordsMatch() -> Bool {
        if passwordTextField.text == confirmPasswordTextField.text {
            passwordsMatch = true
            print("Passwords Match")
        } else {
            informationLabel.text = "Password doesn't match. Try again?"
            passwordsMatch = false
            print("Doesn't match")
        }
        return passwordsMatch
    }
    
    func setButtonView() {
        self.goNextButton.layer.borderWidth = 2
        self.goNextButton.layer.borderColor = CGColor(red: 255, green: 255, blue: 255, alpha: 1)
        self.goNextButton.layer.cornerRadius = 15
    }
    
    
}
