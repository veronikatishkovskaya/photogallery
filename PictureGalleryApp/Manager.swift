import Foundation
import UIKit

enum Keys: String {
    case imageInfo
}

class Manager {
    
    static let shared = Manager()
    
    func saveData(_ data: Photos) {
        var imagesArray = self.loadData()
        imagesArray.append(data)
        UserDefaults.standard.set(encodable: imagesArray, forKey: Keys.imageInfo.rawValue)
    }
    
    func loadData() -> [Photos] {
        guard let pictures = UserDefaults.standard.value([Photos].self, forKey: Keys.imageInfo.rawValue) else {
            return []
        }
        return pictures
    }
    
    
}
