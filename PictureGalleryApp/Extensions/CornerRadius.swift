
import Foundation
import UIKit

extension UIView {
    func cornerRadius() {
        self.layer.cornerRadius = 15
    }
}
