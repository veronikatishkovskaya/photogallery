import Foundation
import UIKit

class Photos: Codable {
    var picture: String?
    var comment: String?
    var like: Bool
    
    internal init(picture: String?, comment: String?, like: Bool) {
        self.picture = picture
        self.comment = comment
        self.like = like
    }
    
    enum Keys: String, CodingKey {
        case picture
        case comment
        case like
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Keys.self)
        picture = try container.decodeIfPresent(String.self, forKey: .picture)
        comment = try container.decodeIfPresent(String.self, forKey: .comment)
        like = try container.decodeIfPresent(Bool.self, forKey: .like)!
        
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: Keys.self)
        try container.encode(self.picture, forKey: .picture)
        try container.encode(self.comment, forKey: .comment)
        try container.encode(self.like, forKey: .like)
    }
    
}

extension UserDefaults {
    
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
           let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
    
}

