import UIKit
import SwiftyKeychainKit

class LoginViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var loginTextView: UIView!
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var cameraView: UIImageView!
    @IBOutlet weak var secondCameraView: UIImageView!
    @IBOutlet weak var thirdCameraView: UIImageView!
    @IBOutlet weak var fourthCameraView: UIImageView!
    @IBOutlet weak var fifthCameraView: UIImageView!
    @IBOutlet weak var sixthCameraView: UIImageView!
    
    // MARK: - let
    let passwordText = ""
    // MARK: - VC life Func
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cameraView.animateImage()
        self.secondCameraView.animateImage()
        self.thirdCameraView.animateImage()
        self.fourthCameraView.animateImage()
        self.fifthCameraView.animateImage()
        self.sixthCameraView.animateImage()
        self.loginButton.cornerRadius()
        self.loginTextView.cornerRadius()
        self.loginTextField.text = passwordText
        self.loginTextField.isSecureTextEntry = true
        self.loginTextField.placeholder = " Enter your Password"
    }
    
    // MARK: - IBAction
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        let controller = SignUpViewController()
        do {
            let value = try controller.keychain.get(controller.accessTokenKey)
            if self.loginTextField.text == value {
                self.switchToPhotoPickerView()
            } else {
                self.invalidPasswordAlert()
            }
        } catch let error {
            debugPrint(error)
        }
        
        
    }
    
    @IBAction func signUpButtonPressed(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: - Flow func
    func invalidPasswordAlert() {
        let alert = UIAlertController(title: "Invalid Password!",
                                      message: "Please, reenter your password",
                                      preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    func switchToPhotoPickerView() {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

// MARK: - Extensions
extension UIImageView {
    func animateImage() {
        UIView.animate(withDuration: 1) {
            self.frame.origin.y += 20
        } completion: { (_) in
            UIView.animate(withDuration: 1) {
                self.frame.origin.y -= 20
            } completion: { (_) in
                self.animateImage()
            }
        }
    }
}
