
import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoView: UIImageView!
    
    func configure(with object: UIImage) {
        photoView.image = object
        photoView.contentMode = .scaleAspectFill
    }
}
