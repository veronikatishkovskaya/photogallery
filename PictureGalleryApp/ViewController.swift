import UIKit

class ViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var addPhotoButtonView: UIButton!
    @IBOutlet weak var nextButtonView: UIButton!
    @IBOutlet weak var backButtonView: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - let
    let imagePicker = UIImagePickerController()
    
    // MARK: - var
    var imagesCollectionArray: [UIImage] = []
    
    // MARK: - VC life Func
    override func viewDidLoad() {
        super.viewDidLoad()
        addPhotoButtonView.cornerRadius()
        backButtonView.cornerRadius()
        nextButtonView.cornerRadius()
        loadImagesToCollectionView()
    }
    
    // MARK: - IBAction
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func goToPictureGallery(_ sender: UIButton) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "PictureGalleryViewController") as? PictureGalleryViewController else {
            return
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func pickPhoto(_ sender: UIButton) {
        performImagePicker()
    }
    
    // MARK: - Flow func
    private func performImagePicker() {
        imagePicker.delegate = self
        imagePicker.modalPresentationStyle = .currentContext
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    func loadImagesToCollectionView() {
        let manager = FileManagerClass()
        let pictures = Manager.shared.loadData().map({$0.picture})
        for selectedPicture in pictures {
            if let convertedPicture = manager.loadImage(fileName: selectedPicture!) {
                imagesCollectionArray.append(convertedPicture)
            }
        }
    }
    
}

// MARK: - Extensions
extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[.originalImage] as? UIImage {
            
            var images: [Photos] = []
            let manager = FileManagerClass()
            if let pickedImage = manager.saveImage(image: pickedImage){
                let currentImage = Photos(picture: pickedImage, comment: nil, like: false)
                
                if let previousImage = UserDefaults.standard.value([Photos].self, forKey: Keys.imageInfo.rawValue) {
                    images = previousImage
                    images.append(currentImage)
                } else {
                    images.append(currentImage)
                }
            }
            UserDefaults.standard.set(encodable: images, forKey: Keys.imageInfo.rawValue)
            imagesCollectionArray.append(pickedImage)
            picker.dismiss(animated: true, completion: nil)
            self.collectionView.reloadData()
        }
    }
    
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesCollectionArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCollectionViewCell", for: indexPath) as? CustomCollectionViewCell else {
            return UICollectionViewCell()
        }
        
        if !imagesCollectionArray.isEmpty {
            cell.configure(with: imagesCollectionArray[indexPath.item])
        } else {
            return cell
        }
        return cell
    }
}

func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let side: CGFloat = (collectionView.frame.size.width - 10) / 2
    return CGSize(width: side, height: side)
}

